package com.example.rojasmigue.vista.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.rojasmigue.R;

public class ActividadLogin extends AppCompatActivity implements View.OnClickListener {
    EditText cajausuario, cajaclave;
    Button boton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_login);
        cargaComponentes();
    }

    private void cargaComponentes(){
        cajausuario = findViewById(R.id.txtUsuario);
        cajaclave = findViewById(R.id.txtclave);
        boton = findViewById(R.id.btningresar);
        boton.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        Toast.makeText(ActividadLogin.this,"Usuario" + cajausuario.getText() + "Clave" + cajaclave.getText(),Toast.LENGTH_SHORT).show();

    }
}
