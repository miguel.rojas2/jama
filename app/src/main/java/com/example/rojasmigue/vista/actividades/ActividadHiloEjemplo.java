package com.example.rojasmigue.vista.actividades;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.rojasmigue.R;
import com.example.rojasmigue.controlador.ServicioEjemplo;
import com.example.rojasmigue.modelo.Ejemplo;
import com.example.rojasmigue.vista.adapter.Ejemplo_Adapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ActividadHiloEjemplo extends AppCompatActivity implements View.OnClickListener {
    EditText cajaDocumento,cajaNombre,cajaProfesion;
    Button botoncalcular, botonGuardar, botonListar, botonModificar, botonEliminar, botonBuscar;
    RecyclerView recyclerUsuarios;
    List<Ejemplo> listaUsuarios;
    Ejemplo_Adapter adapter;

    //Se define la URL del servicio
    String host="http://192.168.1.10/Rojas_Miguel/miguel_alexander/";
    String getAll="/wsJSONConsultarLista.php";
    String insert="/wsJSONRegistro.php";
    String update="/wsJSONUpdateMovil.php";
    String getById="/wsJSONConsultarUsuario.php";
    String delete="/wsJSONDeleteMovil.php";
    //Objeto de tipo servicio web

    ServicioEjemplo sw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_hilo_ejemplo);
        cargarComponentes();
    }

    private void cargarComponentes(){
        recyclerUsuarios=findViewById(R.id.recyclerUsuarioHilo);
        cajaDocumento=findViewById(R.id.txtDocumentoUsuarioHilo);
        cajaNombre=findViewById(R.id.txtNombreUsuarioHilo);
        cajaProfesion=findViewById(R.id.txtProfesionUsuarioHilo);
        botonGuardar=findViewById(R.id.btnguardarejemplo);
        botonListar=findViewById(R.id.btnlistarejmplo);
        botonModificar=findViewById(R.id.btneditarejemplo);
        botonBuscar=findViewById(R.id.btnverejemplo);
        botonEliminar=findViewById(R.id.btneliminarejemplo);
        botoncalcular=findViewById(R.id.btnCalcular);
        botonGuardar.setOnClickListener(this);
        botonListar.setOnClickListener(this);
        botonModificar.setOnClickListener(this);
        botonBuscar.setOnClickListener(this);
        botonEliminar.setOnClickListener(this);
        botoncalcular.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        sw= new ServicioEjemplo();
        switch (v.getId())
        {
            case R.id.btnCalcular:
                int valor1=Integer.parseInt(cajaDocumento.getText().toString());
                int valor2=Integer.parseInt(cajaNombre.getText().toString());
                int suma=valor1+valor2;
                cajaProfesion.setText(suma+"");

            case R.id.btnlistarejmplo:
                try {
                    String cadena = sw.execute(host.concat(getAll),"1").get();  //Ejecución del hilo en el doINBackGround
                    JsonParse(cadena);
                    Toast.makeText(this,"Se han listado correctamente",Toast.LENGTH_SHORT).show();
                    //limpiar();
                } catch (Exception e){
                    Log.e("Error:",e.getMessage());
                }
                break;
            case R.id.btnguardarejemplo:
                try {

                    sw.execute(host.concat(insert), "2", cajaDocumento.getText().toString(), cajaNombre.getText().toString(),cajaProfesion.getText().toString()).get();


                }
                catch (Exception e)
                {
                    Log.e("Error:",e.getMessage());
                }
                Toast.makeText(this,"Se ha guardado correctamente",Toast.LENGTH_SHORT).show();
                break;
            case R.id.btneditarejemplo:
                try {
                    sw.execute(host.concat(update), "3", cajaDocumento.getText().toString(), cajaNombre.getText().toString(),cajaProfesion.getText().toString()).get();
                } catch (Exception e){
                    Log.e("Error:",e.getMessage());
                }
                Toast.makeText(this,"Se ha modificado correctamente",Toast.LENGTH_SHORT).show();
                break;
            case R.id.btneliminarejemplo:
                sw.execute(host.concat(delete),"4",cajaDocumento.getText().toString());
                Toast.makeText(this,"Se ha eliminado correctamente",Toast.LENGTH_SHORT).show();
                limpiar();
                break;
            case R.id.btnverejemplo:
                try {
                    String cadena=sw.execute(host.concat(getById),"5",cajaDocumento.getText().toString()).get();
                    JsonParse(cadena);
                    limpiar();
                } catch (Exception e){
                    Log.e("Error:",e.getMessage());
                }
                break;
        }
    }


    private void JsonParse(String cadena) throws JSONException {
        JSONObject jsonObject = new JSONObject(cadena);
        JSONArray jsonArray = jsonObject.getJSONArray("usuario");
        listaUsuarios=new ArrayList<Ejemplo>();
        for (int i=0 ; i<jsonArray.length() ; i++){
            JSONObject ejemplos = jsonArray.getJSONObject(i);
            Ejemplo ejemplo = new Ejemplo();
            ejemplo.setDocumento(ejemplos.getInt("documento"));
            ejemplo.setNombre(ejemplos.getString("nombre"));
            ejemplo.setProfesion(ejemplos.getString("profesion"));

            listaUsuarios.add(ejemplo);
            adapter = new Ejemplo_Adapter(listaUsuarios);
            recyclerUsuarios.setLayoutManager(new LinearLayoutManager(ActividadHiloEjemplo.this));
            adapter.setOnclickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cargarCajasdeTexto(v);
                }
            });
            recyclerUsuarios.setAdapter(adapter);
        }
    }
    private void cargarCajasdeTexto(View view) {
        cajaDocumento.setText(""+listaUsuarios.get(recyclerUsuarios.getChildAdapterPosition(view)).getDocumento());
        cajaNombre.setText(""+listaUsuarios.get(recyclerUsuarios.getChildAdapterPosition(view)).getNombre());
        cajaProfesion.setText(""+listaUsuarios.get(recyclerUsuarios.getChildAdapterPosition(view)).getProfesion());
    }
    private void limpiar(){
        cajaDocumento.setText("");
        cajaNombre.setText("");
        cajaProfesion.setText("");
    }


}
