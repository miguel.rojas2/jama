package com.example.rojasmigue.vista.actividades;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.rojasmigue.R;
import com.example.rojasmigue.modelo.Artista;
import com.example.rojasmigue.vista.adapter.ArtistaAdapter;

import java.util.ArrayList;
import java.util.List;

public class ActividadRecyclerArtistas extends AppCompatActivity  {

    Button botonDatos;
    RecyclerView recyclerViewArtistas;
    ArtistaAdapter adapter;
    List<Artista> listaArtistas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activivdad_recycler_artistas);
        tomarControl();
        cargarRecycler();
    }


    private void tomarControl(){
        recyclerViewArtistas = findViewById(R.id.recyclerArtista);
    }
    private void cargarRecycler(){
        Artista artista1 = new Artista();
        artista1.setNombres("Enrrique");
        artista1.setApellidos("Igleias");
        artista1.setNombreArtistico("quike");
        artista1.setImgfoto(R.drawable.enrique);

        Artista artista2 = new Artista();
        artista2.setNombres("Taylor");
        artista2.setApellidos("Sfiwf");
        artista2.setNombreArtistico("tay");
        artista2.setImgfoto(R.drawable.taylor);

        Artista artista3 = new Artista();
        artista3.setNombres("Scarlet");
        artista3.setApellidos("Joason");
        artista3.setNombreArtistico("the black widown");
        artista3.setImgfoto(R.drawable.escar);


        listaArtistas = new ArrayList<Artista>();
        listaArtistas.add(artista1);
        listaArtistas.add(artista2);
        listaArtistas.add(artista3);

        adapter = new ArtistaAdapter(listaArtistas);
        recyclerViewArtistas.setLayoutManager(new LinearLayoutManager(this));
        adapter.setOnclickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DetallesArtistas(v);
            }
        });
        recyclerViewArtistas.setAdapter(adapter);

    }
    public void DetallesArtistas(View view){
        Dialog dlgDetalles = new Dialog(ActividadRecyclerArtistas.this);
        dlgDetalles.setContentView(R.layout.dlg_datos_artistas);
        TextView nombresDetalles = dlgDetalles.findViewById(R.id.lblDatos1);
        TextView apellidosDetalles = dlgDetalles.findViewById(R.id.lblDatos2);
        ImageView fotoDetalles= dlgDetalles.findViewById(R.id.imgDlgDetalles);
        nombresDetalles.setText("Nombres: "+ listaArtistas.get(recyclerViewArtistas.getChildAdapterPosition(view)).getNombres());
        apellidosDetalles.setText("Apellidos: "+ listaArtistas.get(recyclerViewArtistas.getChildAdapterPosition(view)).getApellidos());
        fotoDetalles.setImageResource(listaArtistas.get(recyclerViewArtistas.getChildAdapterPosition(view)).getImgfoto());
        dlgDetalles.show();
    }
}

