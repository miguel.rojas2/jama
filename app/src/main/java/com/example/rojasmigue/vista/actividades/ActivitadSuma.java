package com.example.rojasmigue.vista.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rojasmigue.R;

public class ActivitadSuma extends AppCompatActivity implements View.OnClickListener {
    EditText numero1, numero2,respuesta;
    TextView resultado;
    Button boton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activitad_suma);
        cargarcomponente();
    }
    public void cargarcomponente()
    {
        numero1 = findViewById(R.id.txtnumero1);
        numero2 = findViewById(R.id.txtnumero2);
        respuesta=findViewById(R.id.txtrespuestasuma);
        resultado = findViewById(R.id.lblrespuestasuma);
        boton = findViewById(R.id.btnsuma);
        boton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int valor1 = Integer.parseInt(numero1.getText().toString());
        int valor2 = Integer.parseInt(numero2.getText().toString());
        int suma = valor1 + valor2;
        respuesta.setText(suma + "");
    }

}
