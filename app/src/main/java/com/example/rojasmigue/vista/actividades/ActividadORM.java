package com.example.rojasmigue.vista.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.rojasmigue.R;
import com.example.rojasmigue.modelo.Carro;
import com.example.rojasmigue.vista.adapter.CarroAdapter;

import java.util.List;

import static com.example.rojasmigue.modelo.Carro.deleteAll;


public class ActividadORM extends AppCompatActivity implements View.OnClickListener {
    EditText cajaPlaca, cajaModel, cajaMarca, cajaAnio;
    Button botonGuardar, botonListar, botonModificar, botonEliminar, botonBuscar, botonEliminarTodo;
    RecyclerView recyclerCarro;
    List<Carro> listaCarros;
    CarroAdapter adapter;

    public  void listarCarros()
    {
        try {
            Carro carroL= new Carro();
            listaCarros = carroL.getAllCars();
            adapter = new CarroAdapter(listaCarros);
            recyclerCarro.setLayoutManager(new LinearLayoutManager(this));
            adapter.setOnclickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cargarCajasdeTexto(v);
                }
            });
            recyclerCarro.setAdapter(adapter);
        }catch (Exception ex){
            Log.e("error", ex.getMessage());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_orm);
        cargarComponentes();
        listarCarros();
    }
    private void cargarComponentes(){
        recyclerCarro=findViewById(R.id.recyclerORM);
        cajaPlaca=findViewById(R.id.txtplacaCarro);
        cajaModel=findViewById(R.id.txtmodeloCarro);
        cajaMarca=findViewById(R.id.txtmarcaCarro);
        cajaAnio=findViewById(R.id.txtanioCarro);
        botonGuardar=findViewById(R.id.btnguardarORM);
        botonModificar=findViewById(R.id.btnModificarDBORM);
        botonEliminar=findViewById(R.id.btnEliminarDBORM);
        botonEliminarTodo=findViewById(R.id.btnEliminartodoDBORM);
        botonGuardar.setOnClickListener(this);
        botonModificar.setOnClickListener(this);
        botonEliminar.setOnClickListener(this);
        botonEliminarTodo.setOnClickListener(this);
    }
    public void limpiar(){
        cajaPlaca.setText("");
        cajaMarca.setText("");
        cajaModel.setText("");
        cajaAnio.setText("");
    }
    private void cargarCajasdeTexto(View v) {
        cajaPlaca.setText(""+listaCarros.get(recyclerCarro.getChildAdapterPosition(v)).getPlaca());
        cajaModel.setText(""+listaCarros.get(recyclerCarro.getChildAdapterPosition(v)).getModelo());
        cajaMarca.setText(""+listaCarros.get(recyclerCarro.getChildAdapterPosition(v)).getMarca());
        cajaAnio.setText(""+listaCarros.get(recyclerCarro.getChildAdapterPosition(v)).getAnio());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnguardarORM:
                try {
                    Carro carros= new Carro();
                    carros.setPlaca(cajaPlaca.getText().toString());
                    carros.setMarca(cajaMarca.getText().toString());
                    carros.setModelo(cajaModel.getText().toString());
                    carros.setAnio(Integer.parseInt(cajaAnio.getText().toString()));
                    carros.save();
                    limpiar();
                    listarCarros();
                    Toast.makeText(this, "Se ha guardado correctamente", Toast.LENGTH_SHORT).show();
                }catch (Exception ex){
                    Carro carros= new Carro();
                    carros.setPlaca(cajaPlaca.getText().toString());
                    carros.setMarca(cajaMarca.getText().toString());
                    carros.setModelo(cajaModel.getText().toString());
                    carros.setAnio(Integer.parseInt(cajaAnio.getText().toString()));
                    carros.save();
                    limpiar();
                    listarCarros();
                    Toast.makeText(this, "si sw gurdo campos están vacios", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btnModificarDBORM:
               modificarCarro();
                listarCarros();
                break;
            case R.id.btnEliminarDBORM:
                eliminarCarros();
                listarCarros();
                break;
            case R.id.btnEliminartodoDBORM:
                try {
                    limpiar();
                    deleteAll();
                    listarCarros();
                }catch (Exception ex){
                    Log.e("error",ex.getMessage());
                }
                break;

        }

    }



    private void modificarCarro()
    {
        try {
            Carro carroM= new Carro();
            carroM.modifyCar(cajaPlaca.getText().toString(),cajaModel.getText().toString(),cajaMarca.getText().toString(),Integer.parseInt(cajaAnio.getText().toString()));
            limpiar();
            Toast.makeText(this, "Se ha modificado correctamente", Toast.LENGTH_SHORT).show();
        }catch (Exception ex){
            Toast.makeText(this, "Ingrese la placa del vehículo que desea modificar", Toast.LENGTH_SHORT).show();
        }
    }
    public  void eliminarCarros() {
        try {
            Carro carroD = new Carro();
            carroD.deleteCar(cajaPlaca.getText().toString());
            limpiar();
            Toast.makeText(this, "Se ha eliminado correctamente", Toast.LENGTH_SHORT).show();
        } catch (Exception ex) {
            Toast.makeText(this, "Ingrese la placa del vehículo que desea eliminar", Toast.LENGTH_SHORT).show();

        }

    }

}
