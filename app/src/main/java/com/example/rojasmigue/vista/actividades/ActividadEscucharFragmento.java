package com.example.rojasmigue.vista.actividades;

import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.example.rojasmigue.R;
import com.example.rojasmigue.modelo.Comunicador;
import com.example.rojasmigue.vista.fragmentos.FrgEnviar;
import com.example.rojasmigue.vista.fragmentos.FrgRecibir;

public class ActividadEscucharFragmento extends AppCompatActivity implements Comunicador, FrgRecibir.OnFragmentInteractionListener, FrgEnviar.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_escuchar_fragmento);
    }

    @Override
    public void responder(String datos) {
        FragmentManager fragmentManager =getSupportFragmentManager();
        FrgRecibir frg =(FrgRecibir) fragmentManager.findFragmentById(R.id.FrgRecibir);
        frg.cambiarTexto(datos);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
