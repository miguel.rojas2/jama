package com.example.rojasmigue.vista.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rojasmigue.R;

public class Activiti_Sensores extends AppCompatActivity  implements SensorEventListener {
    Button btnseguir;
    TextView label1,label2,label3;
    SensorManager manager;
    Sensor sensor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activiti__sensores);
        cargarcomponente();
        manager=(SensorManager) getSystemService(SENSOR_SERVICE);//SE OBTIENE CUALQUIER TIPO DE SENSOR
        sensor=manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);//define el tipo de sensor

    }
    public  void  cargarcomponente()
    {
        label1=findViewById(R.id.lblsensor1);
        label2=findViewById(R.id.lblsensor2);
        label3=findViewById(R.id.lblsensor3);


    }




    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        float a,b,c;
        a=sensorEvent.values[0];
        b = sensorEvent.values[1];
        c = sensorEvent.values[2];
        label1.append(a+"");
        label2.append(b+"");
        label3.append(c+"");

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }




    @Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(this, "se llego al on start", Toast.LENGTH_SHORT).show();

    }
    @Override
    protected void onResume() {
        super.onResume();
        manager.registerListener(this,sensor,manager.SENSOR_DELAY_NORMAL);
       // Toast.makeText(this, "se llego al onresumen", Toast.LENGTH_SHORT).show();



    }

    @Override
    protected void onPause() {
        super.onPause();
        manager.unregisterListener(this);//evita el consumo de energia innecesario;
       // Toast.makeText(this, "se llego al onpause", Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onStop() {
        super.onStop();
        Toast.makeText(this, "se llego al onstoṕ", Toast.LENGTH_SHORT).show();


    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "se llego al ondestroy", Toast.LENGTH_SHORT).show();


    }


}
