package com.example.rojasmigue.vista.actividades;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.rojasmigue.R;
import com.example.rojasmigue.controlador.ControladorCoordenadas;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ActividadswCoordenadas extends AppCompatActivity implements View.OnClickListener {
    TextView datos;
    Button botonGuardar;

    String host = "https://samples.openweathermap.org";
    String get="/data/2.5/weather?id=2172797&appid=b6907d289e10d714a6e88b30761fae22";
    RequestQueue mQueue;
    ControladorCoordenadas swC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividadsw_coordenadas);
        mQueue= Volley.newRequestQueue(this);
        cargarComonentes();

    }
    public  void cargarComonentes()
    {
        datos=findViewById(R.id.txtdatosCOOR);
        botonGuardar=findViewById(R.id.btnnverCoo);
        botonGuardar.setOnClickListener(this);
    }
    private void JsonParse(String url) {
        Log.e("metodo","conectado");
        Log.e("URL", String.valueOf(url));
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("obtiene", response.toString());
                        try {
                            JSONObject object =  response.getJSONObject("coord");
                            Double longitud = object.getDouble("lon");
                            Double Latitud = object.getDouble("lat");

                            JSONArray array = response.getJSONArray("weather");

                            int id = 0;
                            String main = "";
                            String descripcion = "";
                            String icon = "";
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject clima = array.getJSONObject(i);
                                id = clima.getInt("id");
                                main = clima.getString("main");
                                descripcion = clima.getString("description");
                                icon = clima.getString("icon");
                            }
                            JSONObject jsonObject = response.getJSONObject("main");
                            String base = response.getString("base");
                            Log.e("dato","este:  " + base);
                            Double temp = jsonObject.getDouble("temp");
                            int pressure = jsonObject.getInt("pressure");
                            int humedad = jsonObject.getInt("humidity");
                            Double tem_mi = jsonObject.getDouble("temp_min");
                            Double tem_max = jsonObject.getDouble("temp_max");
                            JSONObject wind = response.getJSONObject("wind");
                            Double speed = wind.getDouble("speed");
                            int deg = wind.getInt("deg");
                            JSONObject clouds = response.getJSONObject("clouds");
                            int all = clouds.getInt("all");
                            int dt = response.getInt("dt");
                            JSONObject sys = response.getJSONObject("sys");
                            int type = sys.getInt("type");
                            int IdSys = sys.getInt("id");
                            Double message = sys.getDouble("message");
                            String country = sys.getString("country");
                            String sunrise = sys.getString("sunrise");
                            int vissy = response.getInt("visibility");
                            int sunset =  sys.getInt("sunset");
                            int IdDatos = response.getInt("id");
                            String name = response.getString("name");
                            int cod = response.getInt("cod");

                            datos.setText( "COORDENADAS: " + "\n" + "Longitud: " + longitud + " \n" + "Latitud: " + Latitud + "\n" +
                                    "WEATHER:" + "\n" + "id: " + id+ " " + "Main: " + main +"\n" + "Descripcion: " + descripcion + " " + " Icon: " + icon + "\n" +
                                    "BASE: " + base + "\n" +
                                    "MAIN: " + "\n" +  "temp: " + temp +" "+ "pressure: " + pressure + "\n" + "humedad: " + humedad + "\n"
                                    + "Temperatura minima" + tem_mi + " "+"Temperatura Maxima "+  tem_max+ "\n" + "VISIBILIDAD: " +  vissy + "\n"
                                    +"WIND: " + "\n" + "Speed: " + speed + " " + "deg: " + deg + "\n" +
                                    "CLOUDS: " + "\n" + "All: " +" "+ all +"\n"+  "DT: " + dt + " \n"
                                    +"SYS: " + "\n" + "Tipo: " + type + "ID Sys: " + IdSys +" "+ "Mensaje: " +
                                    message + " "+ "Country: " + country +"\n" + "Sunrise: " + sunrise + " "+
                                    "Sunset: " + sunset + "\n" + "ID: " + IdDatos +
                                    "\n" + "NOMBRE: " + name + "\n" + "COD: " + cod + "\n"
                            );
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("error al listar", e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        mQueue.add(request);
    }



    @Override
    public void onClick(View v) {

        swC = new ControladorCoordenadas();
        swC.execute(host.concat(get), "1");
        String url = host.concat(get);
        JsonParse(url);

        }
    }

