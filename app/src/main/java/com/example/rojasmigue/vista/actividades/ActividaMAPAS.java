package com.example.rojasmigue.vista.actividades;

import androidx.fragment.app.FragmentActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.rojasmigue.R;
import com.example.rojasmigue.controlador.ControladorRutas;
import com.example.rojasmigue.modelo.MapasRutas;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

public class ActividaMAPAS extends FragmentActivity implements OnMapReadyCallback , View.OnClickListener {
    ControladorRutas controlruta;
    private GoogleMap mMap;

    private Button botonSatelite,botonTerreno,botonLibreM;
    List<MapasRutas> listaRutas=new ArrayList<>();
    List<LatLng> puntos=new ArrayList<>();
    MapasRutas ruta= new MapasRutas();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activida_mapas);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        cargarComponentes();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void cargarComponentes()
    {
        botonSatelite=findViewById(R.id.btnSatelite);
        botonTerreno=findViewById(R.id.btnTerreno);
        botonLibreM=findViewById(R.id.btnLIbre);
        botonSatelite.setOnClickListener(this);
        botonTerreno.setOnClickListener(this);
        botonLibreM.setOnClickListener(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        controlruta =new ControladorRutas(this);
        mMap = googleMap;
        listaRutas= controlruta.obtenerRutas();

        for ( MapasRutas  rut:listaRutas) {
            mMap.addMarker(new MarkerOptions().position(new LatLng(rut.getLat(),rut.getLng())).title(rut.getNombre()).snippet(rut.getDetalle()).icon(BitmapDescriptorFactory.fromBitmap(obtenerIconos(rut.getIcono()))));
            puntos.add(new LatLng(rut.getLat(),rut.getLng()));
        }


        LatLngBounds ZOOM = controlruta.Zoom(puntos,mMap);

//        Circle circleInicio = controlruta.trazarCirculo(puntos.get(0),mMap);

//        Circle circleFin = controlruta.trazarCirculo(puntos.get(puntos.size()-1),mMap);

      //  Polyline line = controlruta.trazarPolinea(puntos,mMap);


    }

    public Bitmap obtenerIconos(String iconName){
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(),getResources().getIdentifier(iconName, "drawable", getPackageName()));
        return imageBitmap;
    }



   /* public  void crearPoligono()
    {
        PolylineOptions lineas=new PolylineOptions();
        lineas.add(new LatLng(-4.003193,-79.210677)).width(8).color(Color.YELLOW);
        lineas.add(new LatLng(-4.009436,-79.207506)).width(8).color(Color.YELLOW);
        lineas.add(new LatLng(-4.00481,-79.214998)).width(8).color(Color.YELLOW);
        lineas.add(new LatLng(-4.0032471,-79.211820)).width(8).color(Color.YELLOW);
        mMap.addPolyline(lineas);
    }*/

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnSatelite:
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;
            case R.id.btnTerreno:
                mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                break;
            case R.id.btnLIbre:
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                break;

        }

    }
}
