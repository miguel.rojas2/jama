package com.example.rojasmigue.vista.actividades;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.rojasmigue.R;

public class ActividadRecibirParametros extends AppCompatActivity  {

    TextView nombre, apellido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_recibir_parametros);
        RecibirParametros();
    }
    private void RecibirParametros( ){

        nombre = findViewById(R.id.lblNombreRecibirParametro);
        apellido = findViewById(R.id.lblApellidoRecibirParametro);

        Bundle bundle = this.getIntent().getExtras();
        nombre.setText(bundle.getString("Nombre"));
        apellido.setText(bundle.getString("Apellido"));

    }


}
