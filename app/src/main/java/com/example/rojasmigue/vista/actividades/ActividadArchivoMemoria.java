package com.example.rojasmigue.vista.actividades;

import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.example.rojasmigue.R;
import com.example.rojasmigue.vista.fragmentos.FrgMI;
import com.example.rojasmigue.vista.fragmentos.FrgProgramaArt;
import com.example.rojasmigue.vista.fragmentos.FrgSD;


public class ActividadArchivoMemoria extends AppCompatActivity implements  FrgMI.OnFragmentInteractionListener, FrgSD.OnFragmentInteractionListener, FrgProgramaArt.OnFragmentInteractionListener{

    Button botonMI, botonSD, botonPrograma;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_archivomen);
        cargarComponentes();
    }
    public void cargarComponentes (){

    }
    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.submenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.MemoriaSD:
                FrgSD fragmentoSD = new FrgSD();
                FragmentTransaction transaccion2 = getSupportFragmentManager().beginTransaction();
                transaccion2.replace(R.id.recipiente, fragmentoSD);
                transaccion2.commit();
                break;
            case R.id.MemoriaIn:
                FrgMI fragmentoMI = new FrgMI();
                FragmentTransaction transaccion3 = getSupportFragmentManager().beginTransaction();
                transaccion3.replace(R.id.recipiente, fragmentoMI);
                transaccion3.commit();
                break;


        }
        return true;
    }
}
