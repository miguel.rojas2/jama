package com.example.rojasmigue.vista.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rojasmigue.R;

public class activiti_proximidad extends AppCompatActivity implements SensorEventListener {
    SensorManager manager;
    Sensor sensor;
    TextView label1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activiti_proximidad);
        cargarcomponente();
        manager=(SensorManager) getSystemService(SENSOR_SERVICE);//SE OBTIENE CUALQUIER TIPO DE SENSOR
        sensor=manager.getDefaultSensor(Sensor.TYPE_PROXIMITY);//define el tipo de sensor
    }
    public  void  cargarcomponente() {
        label1 = findViewById(R.id.lblproximidad);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float a;
        a=event.values[0];
        label1.setText(a+"");
        if(a>0) {
            LinearLayout L1 = (LinearLayout) findViewById(R.id.linear_color);
            L1.setBackgroundColor(Color.BLUE);
        }else {
            LinearLayout L1 = (LinearLayout) findViewById(R.id.linear_color);
            L1.setBackgroundColor(Color.RED);


        }






    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }



    @Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(this, "se llego al on start", Toast.LENGTH_SHORT).show();

    }
    @Override
    protected void onResume() {
        super.onResume();
        manager.registerListener(this,sensor,manager.SENSOR_DELAY_NORMAL);
        // Toast.makeText(this, "se llego al onresumen", Toast.LENGTH_SHORT).show();



    }

    @Override
    protected void onPause() {
        super.onPause();
        manager.unregisterListener(this);//evita el consumo de energia innecesario;
        // Toast.makeText(this, "se llego al onpause", Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onStop() {
        super.onStop();
        Toast.makeText(this, "se llego al onstoṕ", Toast.LENGTH_SHORT).show();


    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "se llego al ondestroy", Toast.LENGTH_SHORT).show();


    }
}
