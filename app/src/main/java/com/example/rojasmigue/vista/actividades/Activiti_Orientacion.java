package com.example.rojasmigue.vista.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rojasmigue.R;

public class Activiti_Orientacion extends AppCompatActivity implements SensorEventListener {
    SensorManager manager;
    Sensor sensor;
    TextView label1,label2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activiti__orientacion);
        cargarcomponente();
        manager=(SensorManager) getSystemService(SENSOR_SERVICE);//SE OBTIENE CUALQUIER TIPO DE SENSOR
        sensor=manager.getDefaultSensor(Sensor.TYPE_ORIENTATION);//define el tipo de sensor
    }
    public  void  cargarcomponente()
    {
        label1 = findViewById(R.id.lblori);
        label2 = findViewById(R.id.lblori1);
    }


    @Override
    public void onSensorChanged(SensorEvent event) {

        float a,b;
        a=event.values[0];
        b=event.values[1];
        label1.setText(a+"");
        label2.setText(b+"");
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
    @Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(this, "se llego al on start", Toast.LENGTH_SHORT).show();

    }
    @Override
    protected void onResume() {
        super.onResume();
        manager.registerListener(this,sensor,manager.SENSOR_DELAY_NORMAL);
        // Toast.makeText(this, "se llego al onresumen", Toast.LENGTH_SHORT).show();



    }

    @Override
    protected void onPause() {
        super.onPause();
        manager.unregisterListener(this);//evita el consumo de energia innecesario;
        // Toast.makeText(this, "se llego al onpause", Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onStop() {
        super.onStop();
        Toast.makeText(this, "se llego al onstoṕ", Toast.LENGTH_SHORT).show();


    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "se llego al ondestroy", Toast.LENGTH_SHORT).show();


    }

}
