package com.example.rojasmigue.vista.actividades;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.rojasmigue.R;
import com.example.rojasmigue.controlador.ControladorAlumno;
import com.example.rojasmigue.modelo.Alumnos;
import com.example.rojasmigue.vista.adapter.AlumnoAdapter;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ActividadswAlumnos extends AppCompatActivity   implements View.OnClickListener{
    EditText cajaNombre, cajaDireccion, cajaID;
    TextView datos;
    Button botonGuardar, botonListar, botonModificar, botonEliminar, botonBuscar;
    RecyclerView recyclerAlumnos;
    List<Alumnos> listaAlumnos;
    RequestQueue mQueue;
    AlumnoAdapter adapter;

    String host="http://reneguaman.000webhostapp.com";
    String insert="/insertar_alumno.php";
    String getAll="/obtener_alumnos.php";
    String getById="/obtener_alumno_por_id.php";
    String update="/actualizar_alumno.php";
    String delete="/borrar_alumno.php";
    //Objeto de tipo servicio web
    ControladorAlumno sw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividadsw_alumnos);
        cargarComponentes();
        mQueue = Volley.newRequestQueue(this);
    }


    private void JsonParse(String url){
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url,null,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("alumnos");
                            listaAlumnos=new ArrayList<Alumnos>();
                            for (int i=0 ; i<jsonArray.length() ; i++){
                                JSONObject alumnos = jsonArray.getJSONObject(i);
                                Alumnos alumno = new Alumnos();
                                alumno.setIdalumno(alumnos.getInt("idalumno"));
                                alumno.setNombre(alumnos.getString("nombre"));
                                alumno.setDireccion(alumnos.getString("direccion"));

                                listaAlumnos.add(alumno);
                                adapter = new AlumnoAdapter(listaAlumnos);
                                recyclerAlumnos.setLayoutManager(new LinearLayoutManager(ActividadswAlumnos.this));
                                adapter.setOnclickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        cargarCajasdeTexto(v);
                                    }
                                });
                                recyclerAlumnos.setAdapter(adapter);
                            }
                        } catch (JSONException ex){
                            Log.e("Error: ",ex.getMessage());
                        }
                    }
                }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        mQueue.add(request);
    }

    private void JsonParses(String url){
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url,null,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject alumnos = response.getJSONObject("alumno");
                            listaAlumnos=new ArrayList<Alumnos>();
                            Alumnos alumno = new Alumnos();
                            alumno.setIdalumno(alumnos.getInt("idAlumno"));
                            alumno.setNombre(alumnos.getString("nombre"));
                            alumno.setDireccion(alumnos.getString("direccion"));

                            listaAlumnos.add(alumno);
                            adapter = new AlumnoAdapter(listaAlumnos);
                            recyclerAlumnos.setLayoutManager(new LinearLayoutManager(ActividadswAlumnos.this));
                            adapter.setOnclickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    cargarCajasdeTexto(v);
                                }
                            });
                            recyclerAlumnos.setAdapter(adapter);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        mQueue.add(request);
    }

    private void cargarComponentes(){
        recyclerAlumnos=findViewById(R.id.reciclerSW);
        cajaID=findViewById(R.id.txtidsw);
        datos=findViewById(R.id.lbldatos);
        cajaNombre=findViewById(R.id.txtnombresw);
        cajaDireccion=findViewById(R.id.txtapellidosw);
        botonGuardar=findViewById(R.id.btnguardarSW);
        botonModificar=findViewById(R.id.btnmodificarSW);
        botonListar=findViewById(R.id.btnlistar);
        botonBuscar=findViewById(R.id.btnbuscarSW);
        botonEliminar=findViewById(R.id.btnelimnarSW);
        botonGuardar.setOnClickListener(this);
        botonModificar.setOnClickListener(this);
        botonListar.setOnClickListener(this);
        botonEliminar.setOnClickListener(this);
        botonBuscar.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        sw = new ControladorAlumno();
        switch (v.getId()){
            case R.id.btnguardarSW:
                Toast.makeText(ActividadswAlumnos.this,"entro", Toast.LENGTH_SHORT);
                sw.execute(host.concat(insert),"1",cajaNombre.getText().toString(),cajaDireccion.getText().toString());//es para poder guardar un dato
                break;
            case R.id.btnlistar:
                 //Ejecución del hilo en el doINBackGround
                sw.execute(host.concat(getAll),"2");
                break;
            case R.id.btnbuscarSW:
                sw.execute(host.concat(getById),"3",cajaID.getText().toString());
                break;
            case R.id.btnmodificarSW:
                sw.execute(host.concat(update),"4",cajaID.getText().toString(),cajaNombre.getText().toString(),cajaDireccion.getText().toString());
                Toast.makeText(this,"Se ha modifico",Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnelimnarSW:
                sw.execute(host.concat(delete),"5",cajaID.getText().toString());
                Toast.makeText(this,"Se ha elimino",Toast.LENGTH_SHORT).show();
                break;

        }


    }
    private void limpiar(){
        cajaID.setText("");
        cajaNombre.setText("");
        cajaDireccion.setText("");
    }
    private void cargarCajasdeTexto(View view) {
        cajaID.setText(""+listaAlumnos.get(recyclerAlumnos.getChildAdapterPosition(view)).getIdalumno());
        cajaNombre.setText(""+listaAlumnos.get(recyclerAlumnos.getChildAdapterPosition(view)).getNombre());
        cajaDireccion.setText(""+listaAlumnos.get(recyclerAlumnos.getChildAdapterPosition(view)).getDireccion());
    }

}
