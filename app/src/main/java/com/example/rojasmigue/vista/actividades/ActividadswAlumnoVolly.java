package com.example.rojasmigue.vista.actividades;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;


import com.example.rojasmigue.R;
import com.example.rojasmigue.controlador.ServicioWebVollyAlumno;
import com.example.rojasmigue.modelo.Alumnos;
import com.example.rojasmigue.vista.adapter.AlumnoAdapter;

import java.util.List;

public class ActividadswAlumnoVolly extends AppCompatActivity  implements View.OnClickListener {
    ServicioWebVollyAlumno sw;
    EditText cajaNombre,cajaDireccion,cajaId;
    Button botonguardar,bontonlistar,botoneliminar,botonmodificar,botonbuscar;
    List<Alumnos> listaAlumnos;
    AlumnoAdapter adapter;
    RecyclerView recyclerAlumnos;
    ServicioWebVollyAlumno swa =new ServicioWebVollyAlumno(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividadsw_alumno_volly);
        cargarComponentes();
    }
    
   public void cargarComponentes()
    {
        recyclerAlumnos=findViewById(R.id.reciclardor);
        cajaId=findViewById(R.id.txtidvolly);
        cajaNombre=findViewById(R.id.txtnombreVolly);
        cajaDireccion=findViewById(R.id.txtdireccionVolly);
        botonguardar=findViewById(R.id.btnguardarvolly);
        bontonlistar=findViewById(R.id.btnlistarvolly);
        botonmodificar=findViewById(R.id.btnmodificarvolly);
        botoneliminar=findViewById(R.id.btneliminavolly);
        botonbuscar=findViewById(R.id.btnbuscarvolly);
        botonguardar.setOnClickListener(this);
        bontonlistar.setOnClickListener(this);
        botonmodificar.setOnClickListener(this);
        botoneliminar.setOnClickListener(this);


    }
    private void cargarCajasdeTexto(View view) {
        cajaId.setText(""+listaAlumnos.get(recyclerAlumnos.getChildAdapterPosition(view)).getIdalumno());
        cajaNombre.setText(""+listaAlumnos.get(recyclerAlumnos.getChildAdapterPosition(view)).getNombre());
        cajaDireccion.setText(""+listaAlumnos.get(recyclerAlumnos.getChildAdapterPosition(view)).getDireccion());
    }
    public void guardar()
    {
        try {
            Alumnos alumno=new Alumnos();
            alumno.setNombre(cajaNombre.getText().toString());
            alumno.setDireccion(cajaDireccion.getText().toString());
            sw.Insertar(alumno);

        }
        catch (Exception ex){}
    }

    public void listartodos()
    {
        try {
            String consulta=swa.ObtenerTodos();

        }
        catch (Exception ex){}


    }
    public void buscaridalum()
    {
        try {
            String consulta=swa.BuscarporID(cajaId.getText().toString());

        }
        catch (Exception ex){}

    }

    public void modificaralumnos()
    {
        try {

            Alumnos sw=new Alumnos();
            sw.setIdalumno(Integer.parseInt(cajaId.getText().toString()));
            sw.setNombre(cajaNombre.getText().toString());
            sw.setDireccion(cajaDireccion.getText().toString());
            swa.Modificar(sw);


        }
        catch (Exception ex){}

    }
    public  void eliminar()
    {
        try {
            sw.Eliminar(cajaId.getText().toString());
        }
        catch (Exception ex)
        {

        }
    }



    @Override
    public void onClick(View v) {
        sw = new ServicioWebVollyAlumno(this);
                switch (v.getId()){
                    case R.id.btnguardarvolly:
                        guardar();
                        break;
                    case R.id.btnlistarvolly:
                        listartodos();
                        break;
                    case R.id.btnbuscarvolly:
                        buscaridalum();
                        break;
                    case R.id.btnmodificarvolly:
                        modificaralumnos();
                        break;
                    case R.id.btneliminavolly:
                        eliminar();
                        break;

        }
    }
}
