package com.example.rojasmigue.vista.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rojasmigue.R;

public class Activiti_luz extends AppCompatActivity implements SensorEventListener {
    SensorManager manager;
    Sensor sensor;
    TextView caja1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activiti_luz);
        cargarcomponente();
        manager = (SensorManager) getSystemService(SENSOR_SERVICE);//SE OBTIENE CUALQUIER TIPO DE SENSOR
        sensor = manager.getDefaultSensor(Sensor.TYPE_LIGHT);//define el tipo de sensor
    }

    public void cargarcomponente() {
        caja1= findViewById(R.id.lbldatoluz);
    }

    @Override
    public void onSensorChanged(SensorEvent  sensorEvent) {
        double a;
        a = sensorEvent.values[0];
        caja1.append(a + "");


    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


    @Override
    protected void onResume() {
        super.onResume();
        manager.registerListener(this, sensor, manager.SENSOR_DELAY_NORMAL);
        // Toast.makeText(this, "se llego al onresumen", Toast.LENGTH_SHORT).show();


    }

    @Override
    protected void onPause() {
        super.onPause();
        manager.unregisterListener(this);//evita el consumo de energia innecesario;
        // Toast.makeText(this, "se llego al onpause", Toast.LENGTH_SHORT).show();

    }
}

