package com.example.rojasmigue.vista.actividades;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.rojasmigue.R;
import com.example.rojasmigue.controlador.ServicioEjemploVolly;
import com.example.rojasmigue.modelo.Ejemplo;
import com.example.rojasmigue.vista.adapter.Ejemplo_Adapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActividadVollyEjemplo extends AppCompatActivity  implements View.OnClickListener {
    EditText cajaDocumento,cajaNombre,cajaProfesion;
    Button botonGuardar, botonListar, botonModificar, botonEliminar, botonBuscar;
    RecyclerView recyclerUsuarios;
    List<Ejemplo> listaUsuarios;
    Ejemplo_Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_volly_ejemplo);
        cargarComponentes();
    }
    private void cargarComponentes(){
        recyclerUsuarios=findViewById(R.id.recyclerUsuarioVolly);
        cajaDocumento=findViewById(R.id.txtDocumentoUsuarioVolly);
        cajaNombre=findViewById(R.id.txtNombreUsuarioVolly);
        cajaProfesion=findViewById(R.id.txtProfesionUsuarioVolly);
        botonGuardar=findViewById(R.id.btnsave);
        botonListar=findViewById(R.id.btnlist);
        botonModificar=findViewById(R.id.btnedit);
        botonBuscar=findViewById(R.id.btnsource);
        botonEliminar=findViewById(R.id.btndelete);

        botonGuardar.setOnClickListener(this);
        botonListar.setOnClickListener(this);
        botonModificar.setOnClickListener(this);
        botonBuscar.setOnClickListener(this);
        botonEliminar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        ServicioEjemploVolly sw = new ServicioEjemploVolly(this);
        switch (v.getId()){

            case R.id.btnsave:
                try {
                    final Map<String, String> parametros = new HashMap<>();
                    parametros.put("documento",cajaDocumento.getText().toString());
                    parametros.put("nombre",cajaNombre.getText().toString());
                    parametros.put("profesion",cajaProfesion.getText().toString());
                    sw.Insertar(parametros);
                    limpiar();
                } catch (Exception e){
                    Log.e("Error:",e.getMessage());
                }
                break;
            case R.id.btnlist:
                try {
                    sw.ObtenerTodos(new ServicioEjemploVolly.volleyResponseListener() {
                        @Override
                        public void onError(String message) {

                        }

                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JsonParse(response);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    limpiar();
                } catch (Exception e){
                    Log.e("Error:",e.getMessage());
                }
                break;


            case R.id.btnedit:
                try {
                    final Map<String, String> parametros = new HashMap<>();
                    parametros.put("documento",cajaDocumento.getText().toString());
                    parametros.put("nombre",cajaNombre.getText().toString());
                    parametros.put("profesion",cajaProfesion.getText().toString());
                    sw.Modificar(parametros);
                    limpiar();
                } catch (Exception e){
                    Log.e("Error:",e.getMessage());
                }
                break;
            case R.id.btnsource:
                try {
                    sw.BuscarporID(cajaDocumento.getText().toString(), new ServicioEjemploVolly.volleyResponseListener() {
                        @Override
                        public void onError(String message) {

                        }

                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JsonParse(response);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    limpiar();
                } catch (Exception e){
                    Log.e("Error:",e.getMessage());
                }
                break;
            case R.id.btndelete:
                try {
                    sw.Eliminar(cajaDocumento.getText().toString());
                    Toast.makeText(this, "Se ha eliminado correctamente", Toast.LENGTH_SHORT).show();
                    limpiar();
                } catch (Exception e){
                    Log.e("Error:",e.getMessage());
                }
                break;

        }


    }

    private void JsonParse(JSONObject jsonObject) throws JSONException {
        JSONArray jsonArray = jsonObject.getJSONArray("usuario");
        listaUsuarios=new ArrayList<Ejemplo>();
        for (int i=0 ; i<jsonArray.length() ; i++){
            JSONObject usuarios = jsonArray.getJSONObject(i);
            Ejemplo ejemplo = new Ejemplo();
            ejemplo.setDocumento(usuarios.getInt("documento"));
            ejemplo.setNombre(usuarios.getString("nombre"));
            ejemplo.setProfesion(usuarios.getString("profesion"));

            listaUsuarios.add(ejemplo);
            adapter = new Ejemplo_Adapter(listaUsuarios);
            recyclerUsuarios.setLayoutManager(new LinearLayoutManager(ActividadVollyEjemplo.this));
            adapter.setOnclickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cargarCajasdeTexto(v);
                }
            });
            recyclerUsuarios.setAdapter(adapter);
        }
    }

    private void cargarCajasdeTexto(View view) {
        cajaDocumento.setText(""+listaUsuarios.get(recyclerUsuarios.getChildAdapterPosition(view)).getDocumento());
        cajaNombre.setText(""+listaUsuarios.get(recyclerUsuarios.getChildAdapterPosition(view)).getNombre());
        cajaProfesion.setText(""+listaUsuarios.get(recyclerUsuarios.getChildAdapterPosition(view)).getProfesion());
    }

    private void limpiar(){
        cajaDocumento.setText("");
        cajaNombre.setText("");
        cajaProfesion.setText("");
    }
}
