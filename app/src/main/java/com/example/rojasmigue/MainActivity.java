package com.example.rojasmigue;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import com.example.rojasmigue.vista.actividades.ActividaMAPAS;
import com.example.rojasmigue.vista.actividades.ActividadArchivoMemoria;
import com.example.rojasmigue.vista.actividades.ActividadEnviarParametros;
import com.example.rojasmigue.vista.actividades.ActividadEscucharFragmento;
import com.example.rojasmigue.vista.actividades.ActividadFragmento;
import com.example.rojasmigue.vista.actividades.ActividadHiloEjemplo;
import com.example.rojasmigue.vista.actividades.ActividadLogin;
import com.example.rojasmigue.vista.actividades.ActividadMapas;
import com.example.rojasmigue.vista.actividades.ActividadMemoriaProgramaRe;
import com.example.rojasmigue.vista.actividades.ActividadORM;
import com.example.rojasmigue.vista.actividades.ActividadProductoHe;
import com.example.rojasmigue.vista.actividades.ActividadRecyclerArtistas;
import com.example.rojasmigue.vista.actividades.ActividadVollyEjemplo;
import com.example.rojasmigue.vista.actividades.ActividadswAlumnoVolly;
import com.example.rojasmigue.vista.actividades.ActividadswAlumnos;
import com.example.rojasmigue.vista.actividades.ActividadswCoordenadas;
import com.example.rojasmigue.vista.actividades.ActivitadSuma;
import com.example.rojasmigue.vista.actividades.Activiti_Orientacion;
import com.example.rojasmigue.vista.actividades.Activiti_Sensores;
import com.example.rojasmigue.vista.actividades.Activiti_luz;
import com.example.rojasmigue.vista.actividades.activiti_proximidad;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private AppBarConfiguration mAppBarConfiguration;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow,
                R.id.nav_tools, R.id.nav_share, R.id.nav_send)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // este metodo permite realizar eventos en cada item de los menus
        Intent intent;
        switch (item.getItemId()) {
            case R.id.opcionSuma:
                intent = new Intent(MainActivity.this, ActivitadSuma.class);
                startActivity(intent);
                break;
            case R.id.opcionlogin:
                intent = new Intent(MainActivity.this, ActividadLogin.class);
                startActivity(intent);
                break;
            case R.id.opcionReyes:
                intent = new Intent(MainActivity.this, ActividadEnviarParametros.class);
                startActivity(intent);
                break;
            case R.id.opcionColores:
                intent = new Intent(MainActivity.this, ActividadFragmento.class);
                startActivity(intent);
                break;
            case R.id.opcionContador:
                intent = new Intent(MainActivity.this, ActividadEscucharFragmento.class);
                startActivity(intent);
                break;
            case R.id.opcionArtista:
                intent = new Intent(MainActivity.this, ActividadRecyclerArtistas.class);
                startActivity(intent);
                break;
            case R.id.opcionArchivoReyes:
                intent = new Intent(MainActivity.this, ActividadMemoriaProgramaRe.class);
                startActivity(intent);
                break;
            case R.id.opcionHelper:
                intent = new Intent(MainActivity.this, ActividadProductoHe.class);
                startActivity(intent);
                break;
            case R.id.opcionORM:
                intent = new Intent(MainActivity.this, ActividadORM.class);
                startActivity(intent);
                break;
            case  R.id.opcionSWHilo:
                intent = new Intent(MainActivity.this, ActividadswAlumnos.class);
                startActivity(intent);
                break;
            case  R.id.opcionSWCoor:
                intent = new Intent(MainActivity.this, ActividadswCoordenadas.class);
                startActivity(intent);
                break;
            case  R.id. opcionSWAlumnoVolly:
                intent = new Intent(MainActivity.this, ActividadswAlumnoVolly.class);
                startActivity(intent);
                break;
            case  R.id. opcionSWHiloEjemplo:
                intent = new Intent(MainActivity.this, ActividadHiloEjemplo.class);
                startActivity(intent);
                break;
            case  R.id.  opcionSWVollyEjemplo:
                intent = new Intent(MainActivity.this, ActividadVollyEjemplo.class);
                startActivity(intent);
                break;
            case  R.id.  opcionMapa:
                intent = new Intent(MainActivity.this, ActividaMAPAS.class);
                startActivity(intent);
                break;
            case  R.id.  opcionRuta:
                intent = new Intent(MainActivity.this, ActividadMapas.class);
                startActivity(intent);
                break;
            case  R.id.  opcionsen:
                intent = new Intent(MainActivity.this, Activiti_Sensores.class);
                startActivity(intent);
                break;
            case  R.id.  opcionpro:
                intent = new Intent(MainActivity.this, activiti_proximidad.class);
                startActivity(intent);
                break;
            case  R.id.   opcionluz:
                intent = new Intent(MainActivity.this, Activiti_luz.class);
                startActivity(intent);
                break;
            case  R.id.  opcionorientacion:
                intent = new Intent(MainActivity.this, Activiti_Orientacion.class);
                startActivity(intent);
                break;
            case R.id.opcionsum:
                final Dialog dlgSumar = new Dialog(MainActivity.this);
                dlgSumar.setContentView(R.layout.dlg_sumar);
                final EditText cajaN1 = dlgSumar.findViewById(R.id.txtN1Dg1);
                final EditText cajaN2 = dlgSumar.findViewById(R.id.txtN2Dg1);
                Button botonSumarDlg = dlgSumar.findViewById(R.id.btnSumarDlg);
                botonSumarDlg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int resultado = Integer.parseInt(cajaN1.getText().toString()) +  Integer.parseInt(cajaN2.getText().toString());
                        Toast.makeText(MainActivity.this,"La suma es: "+ resultado, Toast.LENGTH_SHORT).show();
                        dlgSumar.hide();
                    }
                });
                dlgSumar.show();
                break;
            case R.id.opcionmen:
                intent = new Intent(MainActivity.this, ActividadArchivoMemoria.class);
                startActivity(intent);
                break;
        }
        return true;
    }
    @Override
    public void onClick(View v) {
    }

}
