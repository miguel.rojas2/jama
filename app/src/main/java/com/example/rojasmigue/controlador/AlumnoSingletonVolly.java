package com.example.rojasmigue.controlador;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class AlumnoSingletonVolly {
    private RequestQueue queue;
    private Context context;
    private static AlumnoSingletonVolly miInstancia;

    public AlumnoSingletonVolly(Context context) {
        this.context = context;
        //obtener el propio RequestQueue
        //creamos un objet que deveuleva ese valor
        queue=getRequestQueue();//maneja todas las peticiones
    }

    public RequestQueue getRequestQueue()
    {
        if(queue==null)
            queue = Volley.newRequestQueue(context.getApplicationContext());
        return  queue;
    }
    ///////////permite que no se creene objetos y que no se repita
    public static synchronized  AlumnoSingletonVolly getInstance(Context context)
    {
        if(miInstancia==null)
        {
            miInstancia=new AlumnoSingletonVolly(context);

        }
        return miInstancia;
    }

    //generar las lista de pocisiones
    //<T> colleccion
    public <T> void addToRequestque(Request request)
    {
        queue.add(request);
    }
}
