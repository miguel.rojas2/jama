package com.example.rojasmigue.controlador;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ServicioWebClima extends AsyncTask<String, Void, String> {
    JSONObject json = null;

    @Override
    protected String doInBackground(String... parametros) {
        String consulta = "";
        URL url = null;
        String ruta = parametros[0];
        if (parametros[1].equals("1")){
            try {
                url = new URL(ruta);
                HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                int codigoRespuesta = conexion.getResponseCode();
                if (codigoRespuesta == HttpURLConnection.HTTP_OK){
                    InputStream in = new BufferedInputStream(conexion.getInputStream());
                    BufferedReader lector = new BufferedReader(new InputStreamReader(in));
                    consulta += lector.readLine();
                    //Log.e("mensaje ", consulta);
                    Log.e("url",String.valueOf(url));
                }
            }catch (Exception ex){

            }
        }
        return consulta;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }
}
