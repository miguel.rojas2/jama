package com.example.rojasmigue.controlador;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class ControladorAlumno extends AsyncTask<String, Void, String> {

    JSONObject json = null;
    @Override
    protected String doInBackground(String... parametros) {
        String consulta = "";
        URL url = null;
        String ruta = parametros[0];    //esta es la ruta... http://000.../obtener_alumnos.php
        if (parametros[1].equals("1")){
            try {
                url = new URL(ruta);
                HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                int codigoRespuesta = conexion.getResponseCode();
                if (codigoRespuesta == HttpURLConnection.HTTP_OK){
                    InputStream in = new BufferedInputStream(conexion.getInputStream());
                    BufferedReader lector = new BufferedReader(new InputStreamReader(in));
                    consulta += lector.readLine();
                    Log.e("mensaje ", consulta);
                    Log.e("url",String.valueOf(url));
                }
            }catch (Exception ex){

            }
        } else if (parametros[1].equals("2")){
            try {
                url = new URL(ruta);
                HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                conexion.setDoInput(true);
                conexion.setDoOutput(true);
                conexion.setUseCaches(false);
                conexion.connect();

                JSONObject json = new JSONObject();
                json.put("nombre", parametros[2]);
                json.put("direccion", parametros[3]);

                OutputStream os = conexion.getOutputStream();
                BufferedWriter escritor = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                escritor.write(json.toString());
                escritor.flush();
                escritor.close();
                int codigoRespuesta = conexion.getResponseCode();
                if (codigoRespuesta == HttpURLConnection.HTTP_OK) {
                    BufferedReader lector = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
                    consulta += lector.readLine();
                    //Toast.makeText(ActividadSWAlumnos.this, "Guardado", Toast.LENGTH_SHORT).show();
                }
            }catch (Exception ex){
                Log.e("Metodo", "no guarda");
            }

        } else if (parametros[1].equals("3")){
            try {
                url = new URL(ruta);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                int codigo_respuesta = connection.getResponseCode();

                if (codigo_respuesta == HttpURLConnection.HTTP_OK) {
                    InputStream in = new BufferedInputStream(connection.getInputStream());
                    BufferedReader lector = new BufferedReader(new InputStreamReader(in));
                    consulta += lector.readLine();

                    json = new JSONObject(consulta);
                    Log.e("datos", consulta);
                    String ur = String.valueOf(url);
                    Log.e("dat0","mensaje:" +ur);
                }
            } catch (Exception ex) {
                Log.e("error ", "Obtener: " + ex.getMessage());
            }
        } else if (parametros[1].equals("4")) {
            try {
                url = new URL(ruta);
                HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                conexion.setDoInput(true);
                conexion.setDoOutput(true);
                conexion.setUseCaches(false);
                conexion.connect();

                JSONObject json = new JSONObject();
                json.put("idalumno", parametros[2]);
                json.put("nombre", parametros[3]);
                json.put("direccion", parametros[4]);

                OutputStream os = conexion.getOutputStream();
                BufferedWriter escritor = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                escritor.write(json.toString());
                escritor.flush();
                escritor.close();
                int codigoRespuesta = conexion.getResponseCode();
                if (codigoRespuesta == HttpURLConnection.HTTP_OK) {
                    BufferedReader lector = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
                    consulta += lector.readLine();
                    //Toast.makeText(ActividadSWAlumnos.this, "Modificado", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception ex) {
                //Log.e("Metodo", "no modifica");
            }
        } else if (parametros[1].equals("5")){
            try {
                url = new URL(ruta);
                HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                conexion.setDoInput(true);
                conexion.setDoOutput(true);
                conexion.setUseCaches(false);
                conexion.setRequestProperty("Content-Type", "application");
                conexion.setRequestProperty("", "");
                conexion.connect();

                JSONObject json = new JSONObject();
                json.put("idalumno", parametros[2]);

                OutputStream os = conexion.getOutputStream();
                BufferedWriter escritor = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                escritor.write(json.toString());
                escritor.flush();
                escritor.close();
            }catch (Exception ex){

            }
        }
        return consulta;
    }

    @Override
    protected void onPostExecute(String s) {
        //datos.setText(s);
        //super.onPostExecute(s);
    }
}

