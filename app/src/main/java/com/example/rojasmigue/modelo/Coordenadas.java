package com.example.rojasmigue.modelo;

import org.json.JSONException;
import org.json.JSONObject;

public class Coordenadas {
    private double lon;
    private double lat;


    public Coordenadas (){

    }

    public Coordenadas (double lon, double lat) {

        this.lon = lon;
        this.lat = lat;
    }

    public Coordenadas (JSONObject objetoJSON) throws JSONException {
        lon = objetoJSON.getDouble("lon");
        lat = objetoJSON.getDouble("lat");
    }


    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }


}
