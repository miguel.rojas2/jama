package com.example.rojasmigue.modelo;

public interface Comunicador {
    public void responder(String datos);
}
