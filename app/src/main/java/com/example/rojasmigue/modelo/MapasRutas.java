package com.example.rojasmigue.modelo;

public class MapasRutas {
    private double Lat;
    private double Lng;
    private String nombre;
    private String detalle;
    private  String icono;
    private  String desripcion;

    public String getDesripcion() {
        return desripcion;
    }

    public void setDesripcion(String desripcion) {
        this.desripcion = desripcion;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    private  String foto;

    public MapasRutas()
    {
        nombre="";
        Lat=0;
        Lng=0;
        detalle="";
        icono="";
        desripcion="";
        foto="";
    }

    public MapasRutas(double lat, double lng, String nombre,String detalle,String icono,String desripcion,String foto) {
        Lat = lat;
        Lng = lng;
        nombre= nombre;
        detalle= detalle;
        icono = icono;
        desripcion=desripcion;
        foto=foto;
    }
    public double getLat() {
        return Lat;
    }

    public void setLat(double lat) {
        Lat = lat;
    }

    public double getLng() {
        return Lng;
    }

    public void setLng(double lng) {
        Lng = lng;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getIcono() {
        return icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }


}
